<#if car.brand ?exists>
	and brand = :car.brand
</#if>
<#if car.model ?exists>
	and model = :car.model
</#if>
<#if car.power ?exists>
	and power = :car.power
</#if>
<#if car.price ?exists>
	and price = :car.price
</#if>
<#if car.productDate ?exists>
	and productDate = :car.productDate
</#if>