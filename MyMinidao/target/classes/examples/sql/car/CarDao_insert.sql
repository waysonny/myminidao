insert into car (brand,model,power,price,productDate,createBy,createDate) 
values
      (:car.brand,
       :car.model,
       :car.power,
       :car.price,
       :car.productDate,
       :car.createBy,
       :car.createDate
      )