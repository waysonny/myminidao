package test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import test.spring.SpringTxTestCase;
import examples.entity.car.Car;
import examples.dao.car.CarDao;
/**
 * 单元测试
 */
public class CarDaoJunit extends SpringTxTestCase {

	@Resource(name = "carDao")
	private CarDao carDao;

//	@Test
	public void testInsert() {
		logger.info("--------testInsert--------------------------------------------------------------");

		Car car = new Car();
		car.setBrand("Land Rover");
		car.setModel("Discovery");
		car.setPower(200);
		car.setPrice(new BigDecimal(998000));
		car.setProductDate(new Date());
		int count=carDao.insert(car);
		
		logger.info("------update---count---" + count);
	}

//	@Test
	public void testUpdate() {
		logger.info("--------testUpdate--------------------------------------------------------------");

		Car car = new Car();
		car.setId(1);
		car.setBrand("Toyota");
		int num = carDao.update(car);
		
		logger.info("------update---count---" + num);
	}
	
//	@Test
	public void testGetCars() {
		logger.info("--------testGetCars--------------------------------------------------------------");

		Car car = new Car();
		List<Car> carList= carDao.getCars(car);
		
		for (Car c : carList) {
			logger.info(c.getBrand()+"--"+c.getModel()+"--"+c.getPower()+"--"+c.getPrice()+"--"+c.getProductDate());
		}
	}
	
//	@Test
	public void testDelete() {
		logger.info("--------testDelete--------------------------------------------------------------");

		Car car = new Car();
		car.setId(1);
		int count=carDao.delete(car);
		
		logger.info("------delete---count---" + count);
	}
}
