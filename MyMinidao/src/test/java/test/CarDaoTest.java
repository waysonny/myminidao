package test;

import java.math.BigDecimal;
import java.util.Date;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import examples.dao.car.CarDao;


import examples.entity.car.Car;

public class CarDaoTest {
	public static void main(String args[]) {
		BeanFactory factory = new ClassPathXmlApplicationContext("applicationContext.xml");
		CarDao carDao = (CarDao) factory.getBean("carDao");
		
		Car car = new Car();
		car.setBrand("Ford");
		car.setModel("Focus");
		car.setPower(100);
		car.setPrice(new BigDecimal(128000));
		car.setProductDate(new Date());
		
		carDao.insert(car);
	}
}
