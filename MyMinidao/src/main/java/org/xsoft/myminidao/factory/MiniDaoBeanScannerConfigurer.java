package org.xsoft.myminidao.factory;

import java.lang.annotation.Annotation;

import org.xsoft.myminidao.annotation.MiniDao;
import org.xsoft.myminidao.aop.MiniDaoHandler;
import org.xsoft.myminidao.aspect.EmptyInterceptor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.StringUtils;

/**
 * 扫描daoInterface
 * 注册miniDaoHandler
 * 组建动态代理工厂MiniDaoBeanFactory
 * 
 */
public class MiniDaoBeanScannerConfigurer implements BeanDefinitionRegistryPostProcessor {
	/**
	 * Map key类型
	 */
	private String keyType = "origin";
	/**
	 * 是否格式化sql
	 */
	private boolean formatSql = false;
	/**
	 * 是否输出sql
	 */
	private boolean showSql = false;
	/**
	 * 数据库类型
	 */
	private String dbType;
	/**
	 *  dao地址
	 */
	private String basePackage;
	/**
	 * 默认是MiniDao,推荐使用Repository
	 */
	private Class<? extends Annotation> annotation = MiniDao.class;
	/**
	 * Minidao拦截器
	 */
	private EmptyInterceptor emptyInterceptor;
	
	
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	
	public void setFormatSql(boolean formatSql) {
		this.formatSql = formatSql;
	}
	
	public void setShowSql(boolean showSql) {
		this.showSql = showSql;
	}
	
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	
	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}
	
	public void setAnnotation(Class<? extends Annotation> annotation) {
		this.annotation = annotation;
	}
	
	public void setEmptyInterceptor(EmptyInterceptor emptyInterceptor) {
		this.emptyInterceptor = emptyInterceptor;
	}
	
	public EmptyInterceptor getEmptyInterceptor() {
		return emptyInterceptor;
	}

	//@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		//注册Invocation Handler
		registerRequestProxyHandler(registry);

		//指定Scanner扫描的注解
		MiniDaoClassPathMapperScanner scanner = new MiniDaoClassPathMapperScanner(registry, annotation);
		
		//将扫描到的daoInterface，与之前注册的miniDaoHandler，组建动态代理工厂MiniDaoBeanFactory
		scanner.scan(StringUtils.tokenizeToStringArray(this.basePackage, ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS));
	}

	//@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	}

	/**
	 * RequestProxyHandler 手工注册代理类,减去了用户配置XML的烦恼
	 * 
	 * @param registry
	 */
	private void registerRequestProxyHandler(BeanDefinitionRegistry registry) {
		GenericBeanDefinition jdbcDaoProxyDefinition = new GenericBeanDefinition();
		jdbcDaoProxyDefinition.setBeanClass(MiniDaoHandler.class);
		jdbcDaoProxyDefinition.getPropertyValues().add("formatSql", formatSql);
		jdbcDaoProxyDefinition.getPropertyValues().add("keyType", keyType);
		jdbcDaoProxyDefinition.getPropertyValues().add("showSql", showSql);
		jdbcDaoProxyDefinition.getPropertyValues().add("dbType", dbType);
		jdbcDaoProxyDefinition.getPropertyValues().add("emptyInterceptor", emptyInterceptor);
		registry.registerBeanDefinition("miniDaoHandler", jdbcDaoProxyDefinition);//注册Invocation Handler
	}
}
