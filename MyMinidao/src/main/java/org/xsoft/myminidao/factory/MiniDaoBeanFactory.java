package org.xsoft.myminidao.factory;

import java.lang.reflect.Proxy;

import org.xsoft.myminidao.aop.MiniDaoHandler;
import org.springframework.beans.factory.FactoryBean;

/**
 * MiniDaoBeanFactory根据daoInterface，Invocation Handler生产动态代理对象
 * 
 */
public class MiniDaoBeanFactory<T> implements FactoryBean<T> {

	private Class<T> daoInterface; //target

	private MiniDaoHandler proxy; //Invocation Handler
	
	public void setDaoInterface(Class<T> daoInterface) {
		this.daoInterface = daoInterface;
	}
	
	public void setProxy(MiniDaoHandler proxy) {
		this.proxy = proxy;
	}
	
	public MiniDaoHandler getProxy() {
		return proxy;
	}

	//MiniDaoBeanFactory生产动态代理对象
	//@Override
	public T getObject() throws Exception {
		return newInstance();
	}

	//获取动态代理对象的类型
	//@Override
	public Class<?> getObjectType() {
		return daoInterface;
	}

	//单例
	//@Override
	public boolean isSingleton() {
		return true;
	}

	//生成动态代理对象
	@SuppressWarnings("unchecked")
	private T newInstance() {
		return (T) Proxy.newProxyInstance(daoInterface.getClassLoader(), new Class[] { daoInterface }, proxy);
	}

}
