package org.xsoft.myminidao.pojo;

import java.util.List;

/**
 * minidao自动分页设置
 * 
 */
public class MiniDaoPage<T> {
	// 当前页面
	private int page;
	// 每页显示记录数
	private int rows;
	// 总行数
	private int total;
	// 总页数
	private int pages;
	// 结果集
	private List<T> results;

	
	public void setPage(int page) {
		this.page = page;
	}
	
	public int getPage() {
		return page;
	}
	
	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public int getRows() {
		return rows;
	}
	
	public void setTotal(int total) {
		this.total = total;
		this.pages = total / rows + (total % rows > 0 ? 1 : 0);
	}

	public int getTotal() {
		return total;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}
	
	public int getPages() {
		return pages;
	}

	public void setResults(List<T> results) {
		this.results = results;
	}
	
	public List<T> getResults() {
		return results;
	}

}
