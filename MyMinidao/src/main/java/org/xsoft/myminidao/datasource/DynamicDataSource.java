package org.xsoft.myminidao.datasource;

import java.util.Map;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.jdbc.datasource.lookup.DataSourceLookup;

/**
 * 类名：DynamicDataSource.java 功能：动态数据源类
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
	//从配置文件读取数据源Map集合
	@Override
	public void setTargetDataSources(Map<Object, Object> targetDataSources) {
		super.setTargetDataSources(targetDataSources);
	}
	
	//从配置文件读取默认的数据源
	@Override
	public void setDefaultTargetDataSource(Object defaultTargetDataSource) {
		super.setDefaultTargetDataSource(defaultTargetDataSource);
	}
	
	//设置数据源的标识符
	@Override
	public void setDataSourceLookup(DataSourceLookup dataSourceLookup) {
		super.setDataSourceLookup(dataSourceLookup);
	}

	/*
	 * 该方法必须要重写 
	 * 方法是为了根据数据源标示符取得当前的数据源
	 */
	@Override
	protected Object determineCurrentLookupKey() {
		DataSourceType dataSourceType = DataSourceContextHolder.getDataSourceType();
		return dataSourceType;
	}
}
