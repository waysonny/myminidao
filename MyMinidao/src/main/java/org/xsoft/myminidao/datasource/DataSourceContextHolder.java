package org.xsoft.myminidao.datasource;

/**
 * 类名：DataSourceContextHolder.java 功能：获得和设置上下文环境的类，主要负责改变上下文数据源的名称
 * DataSourceContextHolder
 * 
 */
public class DataSourceContextHolder {
	//线程本地环境
	private static final ThreadLocal<DataSourceType> contextHolder = new ThreadLocal<DataSourceType>();
	
	//切换数据源
	public static void setDataSourceType(DataSourceType dataSourceType) {
		contextHolder.set(dataSourceType);
	}
	
	//获取当前数据源
	public static DataSourceType getDataSourceType() {
		return (DataSourceType) contextHolder.get();
	}

	//清除当前数据源
	public static void clearDataSourceType() {
		contextHolder.remove();
	}

}
