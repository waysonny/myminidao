package examples.dao.car;

import java.util.List;

import org.xsoft.myminidao.annotation.Arguments;
import org.springframework.stereotype.Repository;

import examples.entity.car.Car;

@Repository
public interface CarDao {
	
	//插入数据
	@Arguments("car")
	int insert(Car car);
	
	//修改数据
	@Arguments("car")
	int update(Car car);

	//查询数据
	@Arguments({ "car"})
	List<Car> getCars(Car car);
	
	//删除数据
	@Arguments("car")
	int delete(Car car);
	
}
